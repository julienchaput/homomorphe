/*
 implementation d' un cryptosysteme a clef public homomorphe
 l'objectif ici seras d'implementer un cryptosysteme permetant de chiffrer , appliquer des fonctions de façon homomorphe, et déchiffrer.
 De plus il devra ,sans pour autant que cela soit réaliser, être bootstrapable et affin d'obtenir un shema completement homomorphe.

 Pour implementer cela nous utiliserons des réseaux.

 Paramettres:
	- k aleatoire
	- n=2^k
	- f_n(X)=X^n+1
	- v un vecteur de dimension n a coeficient dans Z[X]/f_n(X)
	- w un vecteur de dimension n tel que w*v+u*f_n=d (le coeficient de v pour la relation de bezout entre v et f_n) d doit etre impaire .
	- V la matrice de représentation de la base privé (v0 v1 v2 ... v(n-1))
	       	       	  		       	    	  (-v(n-1) v0 ... v(n-2))
							           .
								   .
								   .
							  (-v1 -v2 ...   v0)
	- r une racine de f_n(X) mod d (c-a-d r^n +1 = 0 mod )
	- B la matrice de représentation de la base public 
	       	       	  		       	    	   (d 0    ...    0)
							   (-r             )
							   (-[r^2]*d       )
							         .   I_(n-1)
								 .
								 .
							   (-[r^(n-1)]*d   )

	
*/

\\ Fonctions:

\\ paramètre de sécurité
param_securite=15;

\\calcule [k]_d
/*
	Prend en entrer un entier,
	renvoie k mod d dans [-d/2,d/2], 
	avec d=pub_key[2] impair
*/
reduce(k) = {
	my(res);
	d=pub_key[2];
	res=k%d;
	if(res>d\2,
		return(res-d);
	);
	return(res);
}

/*
	renvoie [pub_key,key]
	où pub_key=[r,d,n,xi,eth,S,R]
	et key=[wi,d]
*/
init(k,t=500,p=2) = {
	my(n,fn,d,wi,w,v,u,r,pol_v);
	my(S,R,xi,eth,si);
	d=0;
	wi=0;
	n=2^k;
	fn=x^n+1;
	v=vector(n);
	while(wi%p!=1,
		while(d%2==0,
			max_wi=0;
			for(i=1,n-1,
				\\ choisir dans [-2^(t-1),2^t[
				v[i]=random(2^t)-2^(t-1);
			);
			pol_v=Pol(v);
			[w,u,d]=bezout(pol_v,fn);
			if(d!=1,next(2));
			d=Vec(d)[1];
			w=Vec(w);
			for(i=1,#w,
				d=lcm(d,denominator(w[i]));
				if(d%2==0,
					break;
				);
			);
		);
		for(i=1,#w,
			w[i]=abs(w[i])*d;
			if(w[i]>max_wi,max_wi=w[i]);
			if(w[i]%p==1,
				wi=w[i];
			);
		);
		wi=abs(wi);
	);
	add=d\(2*wi+2*p*param_securite*max_wi);
	r=Mod(w[2],d)/Mod(w[1],d);
	S=1000;
	R=Mod(337,d);
	s=15;
	[xi,si]=creation_xi(wi,S,s,R,d);
	eth=creation_etha(si,S);
	return([[r,d,n,xi,eth,S,R],[wi,d]]);
}

/*
	eth=pub_key[5];
*/
chiffre_eth() = {
	for(i=1,matsize(pub_key[5])[1],
		for(j=1,matsize(pub_key[5])[2],
			pub_key[5][i,j]=chiffre(pub_key[5][i,j]);
		);
	);
}

/*
	chiffremnet du bit b avec la clef publique pub_key
*/
chiffre(b,p=2) = {
	my(e,v,c,E,r,d,n);
	r=pub_key[1];
	d=pub_key[2];
	n=pub_key[3];
	\\ paramètres de sécurité
	e=param_securite;
	\\ on tire au hazard e coordonnées parmis n
	E=vector(e,x,n);
	for(i=1,e,
		E[i]=random(n-i+1);
		for(j=1,i-1,
			if(E[j]<=E[i],
				E[i]++;
			);
		);
		E=vecsort(E);
	);
	c=Mod(0,d);
	r=Mod(r,d);
	for(i=1,#E,
		c+=(random(2)*1-1)*r^E[i];
	);
	return(b+p*c);
}

/*
	dechiffrement de c avec la clef privée key
*/
dechiffre(c,p=2) = {
	my(wi,d);
	[wi,d]=key;
	return(reduce(lift(c*wi))%p);
}

\\on renvoi les xi pour i=1,...,s 
creation_xi(w0,S,s,R,d)={
	my(xi=vector(s),tot=0,id=0,si=vector(s),w=0,xs,l,a);
	for(i=1,s-1,
		xi[i]=random(d);
		id=random(S);
		si[i]=id;
		tot+=lift(xi[i]*R^id);
	);
	tot=lift(Mod(w0-tot,d));
	for(i=0,S,[xs,l,a]=gcdext(lift(Mod(R,d)^i),d);if(a==1,xi[s]=xs*tot;si[s]=i));
	[xi,si];	
}

ind(a,b,q)=(a-1)*q-binomial(a,2)+(b-a);

\\renvoi les etha associer au sigma permettant de faire de decrypte dans l'espace des chiffrers
creation_etha(si,S)={
	my(et,si1,q,id,res);
	\\ valeur strictement supérieur à [sqrt(S*2)]
	q=sqrtint(2*S)+2;
	et=matrix(#si,q);
	si1=vector(#si,x,[si[x],x]);
	si1=vecsort(si1,1);
	for(i=1,q,
		for(j=i+1,q,
			id=ind(i,j,q);
			res=vecsearch(si1,[id,0],1);
			if(res!=0 && si[si1[res][2]]==id,
				et[si1[res][2],i]=1;
				et[si1[res][2],j]=1;
			);
		);
	);
	et;
}

pol_sym_aux(l,k,i,m) = {
	my(res);
	res=0;
	if(k==0,return(1));
	for(j=i+1,m-k+1,
		res+=stack_zij[l][j]*pol_sym_aux(l,k-1,j,m);
	);
	return(res);
}

/*
	calcul le polynôme symétrique de degré k en m éléments de stack_zij[l]
*/
pol_sym(l,k,m) = {
	return(pol_sym_aux(l,k,0,m));
}

/*
	pub_key=[r,d,n,xi,eth,S,R]
*/
rechiffre(c) = {
	my(r,d,n,xi,eth,s,S,R,q,rho,s1,s2,s3,ret);
	[r,d,n,xi,eth,S,R]=pub_key;

	/*
		initilisation des paramètres
	*/
	\\ valeur strictement supérieur à [sqrt(S*2)]
	q=sqrtint(2*S)+2;
	s=#xi;
	\\ car s+1 est une puissance de 2 ici
	\\ test avec +1 pour avoir assez de précision
	rho=logint(s+1,2)+1;

	\\ print("GENERATION DES Yij");
	/*
		Generation des y_{i,j}
	*/
	my(y,tmp);
	\\ [1,S]
	y=matrix(s,S);
	for(i=1,s,
		tmp=xi[i]*c;
		for(j=1,S,
			tmp=tmp*R;
			y[i,j]=tmp;
		);
	);
	y=lift(y);

	\\ print("GENERATION DES Zij");
	/*
		Generation des z_{i,j}
	*/
	my(z);
	z=matrix(s,S);
	for(i=1,s,
		for(j=1,S,
			\\ z[i,j] est un vecteur de rho+1 élément
			z[i,j]=digits((lift(y[i,j])*2^rho)\d,2);
			if(#z[i,j]<rho+1,
				z[i,j]=concat(vector(rho+1-#z[i,j]),z[i,j]);
			);
		);
	);

	\\ print("SOMME 1");

	/*
		Calcul de la première somme
	*/
	s1=Mod(0,d);
	for(i=1,s,
		for(j=1,q,
			s2=Mod(0,d);
			for(k=j+1,q,
				if(ind(j,k,q)<=S,
					\\ a modifier [1,S] -> [0,S-1]
					s2+=eth[i,k]*(lift(y[i,ind(j,k,q)])%2);
				);
			);
			s1+=s2*eth[i,j];
		);
	);

	\\ print("SOMME 2");

	/*
		Calcul de la seconde somme
	*/
	stack_zij=vector(rho+1,x,vector(s));
	for(i=1,s,
		s2=Mod(vector(rho+1),d);
		s2_t=Mod(vector(rho+1),d);
		for(j=1,q,
			s3=Mod(vector(rho+1),d);
			s3_t=Mod(vector(rho+1),d);
			for(k=j+1,q,
				if(ind(j,k,q)<=S,
					s3+=eth[i,k]*z[i,ind(j,k,q)];
				);
			);
			s2+=s3*eth[i,j];
		);
		\\print1("Somme2 itération: ");
		\\print(i);
		for(l=1,rho+1,
			stack_zij[l][i]=s2[l];
		);
	);

	\\ print("ADDITION DEBUT");
	
	/*
		addition des z_{i,j}
	*/
	for(l=0,rho-1,
		\\ -j: stack_zij[rho+1-l]
		for(i=l+1,rho,
			\\ -j+delta: stack_zij[rho+1-i]
			if(2^(i-l)>s+l,
				break;
			);
			ret=pol_sym(rho+1-l,2^(i-l),#stack_zij[rho+1-l]);
			stack_zij[rho+1-i]=concat(stack_zij[rho+1-i],ret);
		);
	);

	\\ print("ADDITION FIN");

	/*
		On additionne les resultats de la collones 0 et -1 pour obtenir le résultat
	 	somme(stack_zij[1][l]) + sum(stack_zij[2][l])
	*/
	s2=0;
	s3=0;
	\\ Calcul de la collone 0
	for(l=1,#stack_zij[1],
		s2+=stack_zij[1][l];
	);
	\\ Calcul de la collone -1
	for(l=1,#stack_zij[2],
		s3+=stack_zij[2][l];
	);
	
	\\ résultat de la partie entière supérieur modulo 2
	s2+=s3;

	\\ addition des deux sommes
	return(s1+s2);
}

read_key(filename) = {
	[pub_key,key]=read(filename);
}

write_key(filename) = {
	write(filename,[pub_key,key]);
}

\\fonction pour chiffrer des textes

str2ascii(s)=Vec(Vecsmall(s));
ascii2str(v)=Strchr(v);

encode(s) = {
  [ c-97 | c <- str2ascii(s)];
}

decode(v) = {
  ascii2str([c+97 | c <- v]);
}

\\transformer un texte en un tableau de bits
str_to_byte(text,p)={
	my(base=logint(32,p),res=vector(#text*base),text_int=vector(#text),tmp=vector(base));
	text_int=encode(text);
	for(i=1,#text,tmp=digits(text_int[i],p);for(j=1,#tmp,res[i*base-j+1]=tmp[#tmp-j+1]));
	res;
}

\\transforme un tableau de bits en un text
byte_to_str(tab,p)={
	my(base=logint(32,p),res=vector(#tab/base),tmp=0);
	for(i=1,#res,tmp=0;for(j=1,base,tmp+=tab[base*i-j+1]*p^(j-1));res[i]=tmp);
	res=decode(res);
	res;
}

\\renvoi un tableau d'entier contenant le chiffrer de chaque byte
chiffre_mot(text,p)={
	my(tab_byte=str_to_byte(text,p));
	for(i=1,#tab_byte,tab_byte[i]=chiffre(tab_byte[i],p));
	tab_byte;
}

dechiffre_mot(crypto,p)={
	my(res);
	for(i=1,#crypto,crypto[i]=dechiffre(crypto[i],p));
	res=byte_to_str(crypto,p);
	res;
}

/*
adition de 2 textes chiffrés t1 et t2 de même longueur
adition bit à bit
*/
add_mot(t1,t2)={
	for(i=1,#t1,t1[i]=t1[i]+t2[i]);
	t1;
}

/*
multiplication de 2 textes chiffrés t1 et t2 de même longeur
multiplication bit à bit
*/
mult_mot(t1,t2)={
	for(i=1,#t1,t1[i]=t1[i]*t2[i]);
	t1;
}

\\fonction de test

test_bis(k) = {
	my(m,c1,c2);
	[pub_key,key]=init(k);
	c1=chiffre(0,pub_key);
	c2=chiffre(1,pub_key);
	print();
	print1("n=2^");
	print1(k);
	print1("=");
	print(2^k);
	print1("dechiffre 0: ");
	if(dechiffre(c1,key)==0,print(OK),print(ERREUR));
	print1("dechiffre 1: ");
	if(dechiffre(c2,key)==1,print(OK),print(ERREUR));

	print1("dechiffre 0+0: ");
	if(dechiffre(add(c1,c1,pub_key),key)==0,print(OK),print(ERREUR));
	print1("dechiffre 1+1: ");
	if(dechiffre(add(c2,c2,pub_key),key)==0,print(OK),print(ERREUR));
	print1("dechiffre 0+1: ");
	if(dechiffre(add(c1,c2,pub_key),key)==1,print(OK),print(ERREUR));

	print1("dechiffre 0*0: ");
	if(dechiffre(mult(c1,c1,pub_key),key)==0,print(OK),print(ERREUR));
	print1("dechiffre 1*1: ");
	if(dechiffre(mult(c2,c2,pub_key),key)==1,print(OK),print(ERREUR));
	print1("dechiffre 0*1: ");
	if(dechiffre(mult(c1,c2,pub_key),key)==0,print(OK),print(ERREUR));
}

test()={
	my(wi,r,d,n,b,c,p_key,s_key);
	l=15;
	p=26;
	[p_key,s_key]=init_parametre(l,p);
	t1="patate";
	t2="oobclo";
	ct1=chiffre_mot(t1,p_key,p);
	ct2=chiffre_mot(t2,p_key,p);
	ct=add_mot(ct1,ct2,p_key);
	print(dechiffre_mot(ct,s_key,p));
	print(dechiffre_mot(ct1,s_key,p));
	print(dechiffre_mot(ct2,s_key,p));
	/*
	b=1;
	q=0;
	c=chiffre(b,p_key);
	c1=chiffre(q,p_key);
	b=dechiffre(c,s_key);
	print("b=1");
	print1("b+b= ");
	print(dechiffre(c+c,s_key));
	print1("b*b = ");
	print(dechiffre(c*c,s_key));
	print("q=0");
	print1("q+q = ");
	print(dechiffre(c1+c1,s_key));
	print1("q*q = ");
	print(dechiffre(c1*c1,s_key));
	print1("q+b =");
	print(dechiffre(c+c1,s_key));
	print1("q*b =");
	print(dechiffre(c*c1,s_key));
	*/
}

\\test();

/*
	On veut 2^i > 15
	On doit avoir des erreurs car le paramètre de sécurité est trop gros
	pour n < 8 pour les additions et pour n < 9 pour les multiplications

print("param_securite=15");
for(i=4,9,test_bis(i));

print();
param_securite=1;
print("param_securite=1");
for(i=4,9,test_bis(i));

print();
param_securite=0;
print("param_securite=0");
for(i=4,9,test_bis(i));

*/