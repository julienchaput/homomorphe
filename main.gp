
\\chiffrement d'un premier message m1
sk1=randomprime(2^1000);
r1=random(2^100);
q1=random(2^50);

m1=0;

c1=m1+sk1*r1+2*q1;

\\chiffrement de la clef publique

pk1=sk1+sk1*r1+2*q1;

rp1=random(2^100);
qp1=random(2^50);

\\chiffrer avec la clef secret puis la clef publique 
cp=c1+pk1*qp1+2*qp1;

print((((cp%pk1)%sk1)%2));

\\sk2=randomprime(2^1000);
r2=random(2^100);
q2=random(2^50);

m2=0;

c2=m2+pk1*r2+2*q2;

c3=c2+cp;
c4=c2*cp;

print(((c3%pk1)%sk1)%2);
print(((c4%pk1)%sk1)%2);

\\fonction de chiffrement

Enc(m,key)={
r=random(2^100);
q=random(2^50);
c=m+key*r+2*q;
c;
}

\\fonction de déchiffrement final avec la clef secrete

Dec(c,skey)={
((c%skey)%2);
}

\\Circuit de Déchiffrement
\\listKey est une pile des clef publiques utiliser pour le chiffrement

DecCir(c,listKey)={
dec=c;
for(i=1,#listKey,dec=(dec%listkey[i]));
dec;
}

\\tester avec des messages

m=[1,0,0];
l=[1,0,1];

\\on va aissayer d'aditioner bit a bit ces messages;

cm=vector(3);
cpm=vector(3);
cl=vector(3);

\\chiffrement des deux messages
for(i=1,3,cm[i]=Enc(m[i],sk1);cpm[i]=Enc(cm[i],pk1);cl[i]=Enc(l[i],pk1));

res=vector(3);

\\adition bit a bit des messages
for(i=1,3,res[i]=cpm[i]*cl[i]);

\\circuit de déchiffrement

listkey=[pk1];

for(i=1,3,res[i]=(res[i]%pk1));

\\dechiffrement du message final
for(i=1,3,res[i]=Dec(res[i],sk1));

print(res);
