/*
	Note: le p peut être omis si p=2 dans les différentes fonction
	Note: Pensez à augmenter la stack !
*/

\r Reseau.gp

/*
	Initialisation du cryptosystème
*/

k=6; \\ paramèttre de sécurité
t=500; \\ paramèttre principal déterminant le nombre de multiplication
p=2; \\ espace des clairs Z/pZ

[my_pub_key,my_key]=init(k,t,p); \\ calcul de des clefs du cryptosystème

\\ initilisation des variables globales pub_key et key
[pub_key,key]=[my_pub_key,my_key];

\\ chiffrement des etha
\\chiffre_eth();

\\ la clef publique n'apporte plus aucune information sur la clef privée à présent

\\ Ont peut enregistrer ou lire une paire de clefs

\\ met dans [pub_key,key] la paire de clefs dans le fichier filename
read_key("key6_boot.txt");
\\ write_key(filename);

/*
	Chiffrement & Déchiffrement
*/

b1=random(p); \\ choix d'un message clair
b2=random(p);
print(b1);
print(b2);
c1=chiffre(b1,p); \\ chiffrement
c2=chiffre(b2,p);
dechiffre(c1,p) \\ dechiffrement

/*

exemple de chiffrement d'un mot
m1="patate";
m2="aaaaaa";
c11=chiffre_mot(m1,p); \\chiffrement mot
c22=chiffre_mot(m2,p);

res1=add_mot(c11,c22); \\adition mot bit a bit 
res2=mult_mot(c11,c11); \\multiplication mot bit a bit

dechiffre_mot(res1,p);  \\dechiffrement mot
*/

res=c1+c2; \\ addition
res1=c1*c2; \\ multiplication
print(dechiffre(res,p));
print(dechiffre(res1,p));

/*
	Dechiffrement Homomorphe

	Note: Cette partie ne fonctionne que pour p=2
*/

c1_re=rechiffre(res1); \\ genère un nouveau chiffre de dechiffre(c1)
print(dechiffre(c1_re,p));

\\c11_re=vector(#res1);

\\for(i=1,#res1,c11_re[i]=rechiffre(res1[i])); \\genère #res nouveau chiffre de déchiffre(res1[i]);


quit;