/*
	Note: le p peut être omis si p=2 dans les différentes fonction
	Note: Pensez à augmenter la stack !
*/

\r Reseau.gp

/*
	Initialisation du cryptosystème
*/

k=6; \\ paramèttre de sécurité
t=600; \\ paramèttre principal déterminant le nombre de multiplication
p=2; \\ espace des clairs Z/pZ

[my_pub_key,my_key]=init(k,t,p); \\ calcul de des clefs du cryptosystème

\\ initilisation des variables globales pub_key et key
[pub_key,key]=[my_pub_key,my_key];

\\ chiffrement des etha
chiffre_eth();

\\ la clef publique n'apporte plus aucune information sur la clef privée à présent

\\ Ont peut enregistrer ou lire une paire de clefs

\\ met dans [pub_key,key] la paire de clefs dans le fichier filename
\\read_key("key6_boot.txt");
\\ write_key(filename);

/*
	Chiffrement & Déchiffrement
*/


taille=7;

test=vector(taille);
for(i=1,taille,test[i]=random(p));

print1("vecteur de bit clair originaux : ");
print(test);

for(i=1,taille,test[i]=chiffre(test[i],p));

add=vector(taille);
mul=vector(taille);
for(i=1,taille,add[i]=test[i]+test[i];mul[i]=test[i]*test[i]);

res1=vector(taille);
res2=vector(taille);

for(i=1,taille,res1[i]=dechiffre(add[i],p);res2[i]=dechiffre(mul[i],p));
print1("déchiffrer de la somme bit a bit des chiffrer du vecteur initial : ");
print(res1);
print1("déchiffrer de la multiplication bit à bit des chiffrer du vecteur initial : ");
print(res2);


\\	Dechiffrement Homomorphe

\\	Note: Cette partie ne fonctionne que pour p=2

res_re=vector(taille);
fin=vector(taille);
for(i=1,taille,res_re[i]=rechiffre(test[i]));
print1("vecteur initial après rechiffrement : ");
for(i=1,taille,fin[i]=dechiffre(res_re[i],p));
print(fin);


/*
\\exemple de chiffrement d'un mot

m1="yes";
m2="aaa";
c11=chiffre_mot(m1,p); \\chiffrement mot
c22=chiffre_mot(m2,p);
print1("mot initial : ");
print(dechiffre_mot(c11,p));

res_mot_add=add_mot(c11,c22); \\adition mot bit a bit 
res_mot_mul=mult_mot(c11,c11); \\multiplication mot bit a bit

print1("dechiffrement de la somme bit a bit des chiffrer des bits du mot initial et des bits chiffrer de : ");
print1(m2);
print1(" + ");
print1(m1);
print1(" = ");
print(dechiffre_mot(res_mot_add,p));

print1("dechiffrement de la multiplication bit a bit des chiffrer des bits du mot initial : ")
print1(m1);
print1(" * ");
print1(m1);
print1(" = ");
print(dechiffre_mot(res_mot_add,p));

c11_re=vector(#res_mot_add);
for(i=1,#res_mot_add,c11_re[i]=rechiffre(res_mot_add[i]));
print1(" dechiffrement mot après rechiffre : ");
print(dechiffre_mot(c11_re,p));
*/



quit;